const mysql = require("mysql");
const app = require("express")();
const PORT = process.env.PORT || 1337;

const conn = mysql.createConnection({
  host: 'db',
  user: 'root',
  password: 'root',
  database: 'test_db',
  port: '3306'
});

conn.connect(function (err) {
  if (err) throw err;
  console.log("connected to database!");
});


app.get("/", (req, res) => {
  return res.json({ status: "rendyrket frol..?" });
});

app.listen(PORT, () => {
  console.log(`Running on http://localhost:${PORT}`);
});
